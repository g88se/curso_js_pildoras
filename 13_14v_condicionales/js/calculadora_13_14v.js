var num1 = prompt("Introduce el primer número");
var num2 = prompt("Introduce el segundo número");

if (!isNaN(num1) && !isNaN(num2)) {
  var operacion = prompt(
    "Elije operación, suma resta multiplicación ó división");

  if (operacion == "suma") {
    alert(parseInt(num1) + parseInt(num2));
    console.log(num1);
    console.log(num2);
  } else if (operacion == "resta") {
    alert(parseInt(num1) - parseInt(num2));
    console.log(num1);
    console.log(num2);
  } else if (operacion == "multiplicación" || operacion == "multiplicacion") {
    alert(parseInt(num1) * parseInt(num2));
    console.log(num1);
    console.log(num2);
  } else if (operacion == "división" || operacion == "division") {
    alert(parseInt(num1) / parseInt(num2));
    console.log(num1);
    console.log(num2);
  } else {
    alert("Operación no contemplada");
  }
} else {
  if (typeof num1 == "string" || typeof num2 == "string") {
    alert("Has introducido caracteres y no números");
    console.log(num1);
    console.log(num2);
  } else {
    alert("Error no identificado");
    console.log(num1);
    console.log(num2);
  }
}
