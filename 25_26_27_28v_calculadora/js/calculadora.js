var cifra_de_display = "";
var acumulador_cifra_de_display = 0, is_number;
var primera_operacion = true, sumando = false, restando = false, multiplicando = false, dividiendo = false;

document.getElementById("display").value = 0;

function cal_display(valor_tecla_numero){
  document.getElementById("display").value = cifra_de_display + valor_tecla_numero;
  cifra_de_display = document.getElementById("display").value;
  is_number = parseInt(cifra_de_display);
  console.log("fcal_display var cifra_de_display: " + cifra_de_display);
  console.log("fcal_display var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
  console.log("fcal_display var is_number: " + is_number);
}

function sumar(){
  if (isNaN(is_number)){
    cifra_de_display = "";
  }else{
    if (!primera_operacion){
      if(sumando){
        acumulador_cifra_de_display += parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        console.log("f_sumar_NOPOSUM var cifra_de_display: " + cifra_de_display);
        console.log("f_sumar_NOPOSUM var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_sumar_NOPOSUM var sumando: " + sumando);
      }else if(restando){
        acumulador_cifra_de_display -= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        restando = false;
        console.log("f_sumar_NOPORES var cifra_de_display: " + cifra_de_display);
        console.log("f_sumar_NOPORES var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_sumar_NOPORES var restando: " + restando);
      }else if(multiplicando){
        acumulador_cifra_de_display *= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        multiplicando = false;
        console.log("f_sumar_NOPOMUL var cifra_de_display: " + cifra_de_display);
        console.log("f_sumar_NOPOMUL var acumulador_cifra_de_display: " + acumulador_cifra_de_display);  
        console.log("f_sumar_NOPOMUL var multiplicando: " + multiplicando);
      }else if (dividiendo){
        acumulador_cifra_de_display /= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        dividiendo = false;
        console.log("f_sumar_NOPODIV var cifra_de_display: " + cifra_de_display);
        console.log("f_sumar_NOPODIV var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_sumar_NOPODIV var dividiendo: " + dividiendo);
      }
    }else{
      acumulador_cifra_de_display += parseInt(cifra_de_display);
      document.getElementById("display").value = acumulador_cifra_de_display;
      primera_operacion = false;
      console.log("f_sumar PO var cifra_de_display: " + cifra_de_display);
      console.log("f_sumar PO var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
      console.log("f_sumar PO var sumando: " + sumando);
    }        
    cifra_de_display = "";
    is_number = parseInt(cifra_de_display);
    sumando = true;
    console.log("f_sumar var cifra_de_display: " + cifra_de_display);
    console.log("f_sumar var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
    console.log("f_sumar var is_number: " + is_number);
    console.log("f_sumar var sumando: " + sumando);
    console.log("f_sumar var primera_operacion: " + primera_operacion);
  }
}

function restar(){
  if (isNaN(is_number)){
    cifra_de_display = "";
  }else{
    if(!primera_operacion){
      if(sumando){
        acumulador_cifra_de_display += parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        sumando = false;
        console.log("f_restar_NOPOSUM var cifra_de_display: " + cifra_de_display);
        console.log("f_restar_NOPOSUM var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_restar_NOPOSUM var sumando: " + sumando);
      }else if(restando){
        acumulador_cifra_de_display -= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        console.log("f_restar_NOPORES var cifra_de_display: " + cifra_de_display);
        console.log("f_restar_NOPORES var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_restar_NOPORES var restando: " + restando);
      }else if (multiplicando){
        acumulador_cifra_de_display *= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        multiplicando = false;
        console.log("f_restar_NOPOMUL var cifra_de_display: " + cifra_de_display);
        console.log("f_restar_NOPOMUL var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_restar_NOPOMUL var multiplicando: " + multiplicando);
      }else if (dividiendo){
        acumulador_cifra_de_display /= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        dividiendo = false;
        console.log("f_restar_NOPODIV var cifra_de_display: " + cifra_de_display);
        console.log("f_restar_NOPODIV var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_restar_NOPODIV var dividiendo: " + dividiendo);
      }
    }else{
      acumulador_cifra_de_display += parseInt(cifra_de_display);
      document.getElementById("display").value = acumulador_cifra_de_display;
      primera_operacion = false;
      console.log("f_restar PO var cifra_de_display: " + cifra_de_display);
      console.log("f_restar PO var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
      console.log("f_restar PO var restando: " + restando);     
    }
    cifra_de_display = "";
    is_number = parseInt(cifra_de_display);
    restando = true;
    console.log("f_restar var cifra_de_display: " + cifra_de_display);
    console.log("f_restar var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
    console.log("f_restar var is_number: " + is_number);
    console.log("f_restar var restando: " + restando);
    console.log("f_restar var primera_operacion: " + primera_operacion);
  }  
}

function multiplicar(){
  if(isNaN(is_number)){
    cifra_de_display = "";
  }else{
    if (!primera_operacion){
      if(sumando){
        acumulador_cifra_de_display += parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        sumando = false;
        console.log("f_multiplicar_NOPOSUM var cifra_de_display: " + cifra_de_display);
        console.log("f_multiplicar_NOPOSUM var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_multiplicar_NOPOSUM var sumando: " + sumando);
      }else if(restando){
        acumulador_cifra_de_display -= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        restando = false;
        console.log("f_multiplicar_NOPORES var cifra_de_display: " + cifra_de_display);
        console.log("f_multiplicar_NOPORES var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_multiplicar_NOPORES var restando: " + restando);
      }else if(multiplicando){
        acumulador_cifra_de_display *= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        console.log("f_multiplicar_NOPOMUL var cifra_de_display: " + cifra_de_display);
        console.log("f_multiplicar_NOPOMUL var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_multiplicar_NOPOMUL var multiplicando: " + multiplicando);
      }else if(dividiendo){
        acumulador_cifra_de_display /= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        dividiendo = false;
        console.log("f_multiplicar_NOPODIV var cifra_de_display: " + cifra_de_display);
        console.log("f_multiplicar_NOPODIV var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_multiplicar_NOPODIV var dividiendo: " + dividiendo);
      }
    }else{
      acumulador_cifra_de_display += parseInt(cifra_de_display);
      document.getElementById("display").value = acumulador_cifra_de_display;
      primera_operacion = false;
      console.log("f_multiplicar PO var cifra_de_display: " + cifra_de_display);
      console.log("f_multiplicar PO var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
      console.log("f_multiplicar PO var multiplicando: " + multiplicando);
    }
    cifra_de_display = "";
    is_number = parseInt(cifra_de_display);
    multiplicando = true;
    console.log("f_multiplicar var cifra_de_display: " + cifra_de_display);
    console.log("f_multiplicar var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
    console.log("f_multiplicar var is_number: " + is_number);
    console.log("f_multiplicar var multiplicando: " + multiplicando);
    console.log("f_multiplicar var primera_operacion: " + primera_operacion);
  }
}

function dividir(){
  if(isNaN(is_number)){
    cifra_de_display = "";
  }else{
    if(!primera_operacion){
      if(sumando){
        acumulador_cifra_de_display += parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        sumando = false;
        console.log("f_dividir_NOPOSUM var cifra_de_display: " + cifra_de_display);
        console.log("f_dividir_NOPOSUM var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_dividir_NOPOSUM var sumando: " + sumando);
      }else if(restando){
        acumulador_cifra_de_display -= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        restando = false;
        console.log("f_dividir_NOPORES var cifra_de_display: " + cifra_de_display);
        console.log("f_dividir_NOPORES var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_dividir_NOPORES var restando: " + restando);
      }else if(multiplicando){
        acumulador_cifra_de_display *= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        multiplicando = false;
        console.log("f_dividir_NOPOMUL var cifra_de_display: " + cifra_de_display);
        console.log("f_dividir_NOPOMUL var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_dividir_NOPOMUL var multiplicando: " + multiplicando);
      }else if(dividiendo){
        acumulador_cifra_de_display /= parseInt(cifra_de_display);
        document.getElementById("display").value = acumulador_cifra_de_display;
        console.log("f_divivir_NOPODIV var cifra_de_display: " + cifra_de_display);
        console.log("f_dividir_NOPODIV var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
        console.log("f_dividir NOPODIV var dividiendo: " + dividiendo);
      }
    }else{
      acumulador_cifra_de_display += parseInt(cifra_de_display);
      document.getElementById("display").value = acumulador_cifra_de_display;
      primera_operacion = false;
      console.log("f_dividir PO var cifra_de_display: " + cifra_de_display);
      console.log("f_dividir PO var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
      console.log("f_dividir_PO var dividiendo: " + dividiendo);
    }
    cifra_de_display = "";
    is_number = parseInt(cifra_de_display);
    dividiendo = true;
    console.log("f_dividir var cifra_de_display: " + cifra_de_display);
    console.log("f_dividir var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
    console.log("f_dividir var is_number: " + is_number);
    console.log("f_dividir var dividiendo: " + dividiendo);
    console.log("f_dividir var primera_operacion: " + primera_operacion);    
  }
}

function mostrar_resultado(){
  if(sumando){
    document.getElementById("display").value = acumulador_cifra_de_display + parseInt(cifra_de_display);
    console.log("f_mostrar_sumando var cifra_de_display: " + cifra_de_display);
    console.log("f_mostrar_sumando var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
  }else if(restando){
    document.getElementById("display").value = acumulador_cifra_de_display - parseInt(cifra_de_display);
    console.log("f_mostrar_restando var cifra_de_display: " + cifra_de_display);
    console.log("f_mostrar_restando var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
  }else if(multiplicando){
    document.getElementById("display").value = acumulador_cifra_de_display * parseInt(cifra_de_display);
    console.log("f_mostrar_multiplicando var cifra_de_display: " + cifra_de_display);
    console.log("f_mostrar_multiplicando var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
  }else if(dividiendo){
    document.getElementById("display").value = acumulador_cifra_de_display / parseInt(cifra_de_display);
    console.log("f_mostrar_dividiendo var cifra_de_display: " + cifra_de_display);
    console.log("f_mostrar_dividiendo var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
  }
  acumulador_cifra_de_display = parseInt(document.getElementById("display").value);
  cifra_de_display = 0;
  console.log("f_mostrar_resultado var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
}

function resetear(){
  cifra_de_display = "";
  acumulador_cifra_de_display = 0;
  document.getElementById("display").value = 0;
  primera_operacion = true;
  is_number = parseInt(cifra_de_display);
  console.log("f_resetear var cifra_de_display: " + cifra_de_display);
  console.log("f_resetear var acumulador_cifra_de_display: " + acumulador_cifra_de_display);
  console.log("f_resetear var is_number: " + is_number);
  console.log("f_resetear var primera_operacion: " + primera_operacion);
}