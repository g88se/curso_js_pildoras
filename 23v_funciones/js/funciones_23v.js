//Se puede afinar mas todavia.

let resultado;

function sumar(num1, num2) {
  alert("La suma es: " + (num1 + num2));
  resultado = num1 + num2;
  return resultado;
}

function restar(num1, num2) {
  alert("La resta es: " + (num1 - num2));
  resultado = num1 - num2;
  return resultado;
}

function multiplicar(num1, num2) {
  alert("La multiplicación es: " + num1 * num2);
  resultado = num1 * num2;
  return resultado;
}

function dividir(num1, num2) {
  alert("La divisón es: " + num1 / num2);
  resultado = num1 / num2;
  return resultado;
}

do {
  var operacion = prompt("Elige operación: Sumar, Restar, Multiplicar o dividir").toLowerCase();

  while (!isNaN(operacion) && operacion != "sumar" && operacion != "restar" && operacion != "multiplicar" && operacion != "dividir") {
    operacion = prompt("Error, introduce operación, sumar, restar, multiplicar o dividir correctamente.").toLowerCase();
  }

  var operador1 = parseInt(prompt("Introduce el primer número"));
  //var operador1 = +prompt("Introduce el primer número"); Esta es otra forma para que prompt devuelva un entero
  var operador2 = parseInt(prompt("Introduce el segundo número"));

  switch (operacion) {
    case "sumar":
      sumar(operador1, operador2);
    break;
    case "restar":
      restar(operador1, operador2);
    break;
    case "multiplicar":
      multiplicar(operador1, operador2);
    break;
    case "dividir":
      while(operador1 < operador2){
        alert ("El primer número no puede ser mayor al segundo número en una división");
        operador1 = parseInt(prompt("Introduce el primer número"));
      }
      dividir(operador1, operador2);  
    break;
    default: alert("Operación no permitida");
  }
  var repetir = prompt("¿Deseas repetir?").toLowerCase();
} while (repetir == "si");

document.write("Oprecaión seleccionada: " + operacion + "<br/>");
document.write("Primer número introducido: " + operador1 + "<br/>");
document.write("Segundo número introducido: " + operador2 + "<br/>");
document.write("Resultado de la operación: " + resultado);

console.log(operacion);
console.log(operador1);
console.log(operador2);
console.log(resultado);